terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}


provider "yandex" {
  service_account_key_file = "/Users/ddronov/key.json"
  folder_id                = "b1gkveoveek2evque670"
  cloud_id                 = "b1glakdk9momi90hb5ob"
  zone                     = "ru-central1-b"
}

data "yandex_compute_image" "last_ubuntu" {
  family = "ubuntu-2004-lts" # ОС (Ubuntu, 20.04 LTS)
}

resource "yandex_compute_instance" "web" {
  count = 2
  name  = "nginx${count.index}"

  resources {
    cores         = 2
    memory        = 2
    # core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id = data.yandex_compute_image.last_ubuntu.id
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/yc_ed25519.pub")}"
  }

}
resource "yandex_vpc_network" "network-1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet-1"
  zone           = "ru-central1-b"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}


resource "yandex_lb_target_group" "lb-target" {
  name = "nlb-target"

  target {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    address   = yandex_compute_instance.web[0].network_interface.0.ip_address
  }

  target {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    address   = yandex_compute_instance.web[1].network_interface.0.ip_address
  }
}


resource "yandex_lb_network_load_balancer" "network-balancer" {
  name = "nginx-highly-available-lb"

  listener {
    name = "listener1"
    port = 80
    external_address_spec {
      ip_version = "ipv4"
    }
  }

  attached_target_group {
    target_group_id = yandex_lb_target_group.lb-target.id

    healthcheck {
      name = "http"
      http_options {
        port = 80
        path = "/"
      }
    }
  }
}

output "internal_ip_address_vm" {
  value = yandex_compute_instance.web[*].network_interface.0.ip_address
}
output "external_ip_address_vm" {
  value = yandex_compute_instance.web[*].network_interface.0.nat_ip_address
}




data "yandex_vpc_subnet" "default_b" {
  name = "default-ru-central1-b" # одна из дефолтных подсетей
}

resource "yandex_compute_instance" "my_webserver" {
  name = "reactjs-server"

  resources {
    cores  = 2
    memory = 2
    # core_fraction = 5
  }

  boot_disk {
    initialize_params {
      # ubuntu 20-04
      # image_id = "fd8k5kam36qhmnojj8je"
      image_id = data.yandex_compute_image.last_ubuntu.id
    }
  }

  network_interface {
    #subnet_id = "e2l80221lel3622urnc8"
    subnet_id = data.yandex_vpc_subnet.default_b.subnet_id
    nat       = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/yc_ed25519.pub")}"
  }
}

output "external_ip_address_my_webserver" {
  value = yandex_compute_instance.my_webserver.network_interface.0.nat_ip_address
}
